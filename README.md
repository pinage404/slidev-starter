# Slidev Starter

---

## Usage

In `.gitlab-ci.yml` add:

```yaml
include:
  - remote: "https://gitlab.com/pinage404/slidev-starter/raw/main/.gitlab/ci/pages.gitlab-ci.yml"
```
