FROM node:20-alpine
ENTRYPOINT []
CMD ["/bin/sh"]

ENV CI=true

WORKDIR /slidev
RUN chown node /slidev
USER node

COPY package.json .
COPY package-lock.json .
RUN npm ci --omit=dev
