---
addons:
  - slidev-component-zoom
selectable: true
canvasWidth: 800
lineNumbers: true
---

# Example

---

## Mermaid

```mermaid
%%{init: {'theme':'dark'}}%%
flowchart LR
    Start --> Stop
```

---
src: ./pages/questions.html
---
